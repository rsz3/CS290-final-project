import numpy as np
from random import randint
import re

# Purpose: To bin points to their closest segment
# M x dim matrices s and e containing line segments' start and end points,
# respectively and n x dim matrix pts containing all points to bin
# Does this through finding their closest average distance to each pair of end points
def binPoints(s, e, pts, segNums):
    pts = pts.T
    s = s.T
    e = e.T

    dim = pts.shape[1]
    linePts  = []
    lineVecs = e - s

    dists = np.zeros((s.shape[0], pts.shape[0]))

    #These for loops construct dists, an mxn matrix where entry ij is the
    #distacnce from point j to line segment i. They also initialize linePts,
    #a list of tuples containing a 2xdim matrix that contains a line segment's
    #endpoints, and a 0xdim matrix that will be later filled in with all the
    #points who are closer to that line segment than any other.

    for i in xrange(s.shape[0]):
        print "segment: %d" % i
        #Initialize this line segments entry in linepts to use later.
        line = np.zeros((2, dim))
        line[0,:] = s[i,:]
        line[1,:] = e[i,:]
        linePts.append([line, np.zeros((0, dim)), segNums[i,0]])

        distToA = pts - line[0,:]
        distToB = pts - line[1,:]
        distToA = np.asmatrix(np.linalg.norm(distToA, axis = 1)).T
        distToB = np.asmatrix(np.linalg.norm(distToB, axis = 1)).T
        totalDist = distToA + distToB
        dists[i,:] = totalDist.T

    #Order is mxn matrix with entry ij = 0 iff point j should go in segment i's bin
    print "Have dists"

    closestSeg = np.argmin(dists, axis = 0)
    #Itterate over columns (one point at a time)
    for i in xrange(closestSeg.shape[0]):
        if i%25000 == 0:
            print i

        #Append point i to that line segment's points matrix in linePts.
        linePts[closestSeg[i]][1] = np.concatenate((linePts[closestSeg[i]][1], np.asmatrix(pts[i,:])), axis = 0)

    return linePts

def main():

    for i in [0,10,20,30,40,47,55,65]:
        filename = "./data/NumpyArrays/data_%s.npy"%(i)
        Ps = np.load(filename)[1:4,:] # Load data from the numpy parsed files

        # Limit the number of Points
        selection = [randint(0,Ps.shape[1]-1) for p in range(0, 500000)]
        Ps = Ps[:,selection]           # Limit the number of points

        # Center the Points
        c  = np.asmatrix([list(np.mean(Ps, axis=1))]).T
        Ps = Ps - c

        # Calculate scale
        squares = list(np.einsum("ji,ji->i", Ps, Ps))
        sums    = np.sum(squares)
        scale   = np.sqrt(sums/len(squares))
        Ps      = Ps / scale

        starts = np.zeros((3,0))
        ends   = np.zeros((3,0))

        segNums = np.zeros((1, 0))

        for x in xrange(1,10):
            print x
            # Load and bin points by segment on centerline
            filename1 = "./data/Centerlines/m1Segment_%d.npy"%(x)
            filename2 = "./data/Centerlines/m2Segment_%d.npy"%(x)
            # Centerline
            S2 = np.load(filename1)[:3,:] # Load data from the numpy parsed files
            E2 = np.load(filename2)[:3,:] # Load data from the numpy parsed

            S2 = (S2 - c) / scale # Load data from the numpy parsed files
            E2 = (E2 - c) / scale
            # Center and Scale:

            segNums = np.concatenate((segNums, x * np.ones((1, S2.shape[1]))), axis = 1)

            starts = np.concatenate((starts, S2), axis = 1 )
            ends = np.concatenate((ends,  E2)  , axis = 1 )
        print "Binning %d"%(i)
        print starts.shape
        print ends.shape
        linPts = binPoints(starts, ends, Ps, segNums.T)
        np.save("./data/Centerlines/binnedPoints_%d.npy" % i, linPts)
        print "Done with %s!" % i

if __name__ == "__main__":
    main()
