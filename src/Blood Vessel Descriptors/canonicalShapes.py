import numpy as np
import GeodesicDistance as gd

global distance


def proj(u, v):
    line = u - v
    return (u.dot(v.T) / v.dot(v.T) ) * line

def canonicalShapes(mesh):
    distance = gd.geodesicDistance(mesh)
    fastMDS(mesh)

def selectPoints(distMatrix):
    jRow = np.argmax(distMatrix, axis=0)
    i = np.argmax(jRow)
    j = jRow[i]

    return i,j

def fastMDS(mesh):
    # Select two random
    i,j = selectPoints(distMatrix)
    O1 = mesh.vertices[i]
    O2 = mesh.vertices[j]
    line = mesh.VPos[O2.ID ,:] -  mesh.VPos[O1.ID ,:]

    # Next, all other vertices are projected on that line using the cosine law
    projPts = []
    for pt in xrange(mesh.vertices.shape[0]):
        p = proj(mesh.VPos[O1.ID ,:], mesh.VPos[O2.ID ,:],line)
        projPts.append(p)

    # hyperplane that is perpendicular to line (o1, o2) 3x + 5y + 7z = 0
    orthogonalVector = np.array([[line[0,:]],[line[1,:]],[line[2,:]]])
