import numpy as np

# compute PRST based on Monte Carlo formulation
# X: 3xN matrix holding N points of 3 dimensions
# M: number of random samples
# returns
def PRST_3D(X, M, K):
	N = X.shape[1];
	RAW = np.zeros((M*M,4))
	counter = 0
	for ii in np.random.randint(0, N-1, M):
		for jj in np.random.randint(0, N-1, M):
			v1 = X[:,ii]
			v2 = X[:,jj]
			theta, phi, r = getReflectionPlane(v1, v2)
			d = np.linalg.norm(v2 - v1)
			RAW[counter] = [theta, phi, r, d]
			counter += 1
	tMax = np.max(RAW[:,0])
	pMax = np.max(RAW[:,1])
	rMax = np.max(RAW[:,2])
	PRST = np.zeros((K,K,K))
	for ii in range(RAW.shape[0]):
		tIdx = getIndex(K, RAW[ii,0], tMax)
		pIdx = getIndex(K, RAW[ii,1], pMax)
		rIdx = getIndex(K, RAW[ii,2], rMax)
		PRST[tIdx, pIdx, rIdx] += 1/(2 * RAW[ii,3]**2 * np.sin(theta))
	return np.sqrt(PRST)

def getIndex(K, val, maxVal):
	return np.floor(val/float(maxVal) * (K-1))

# compute plane of reflection in spherical coordinates of their normals:
# \theta \in [0, pi/2], \phi \in [0, 2pi], r \in [-rmax, rmax]
# returns tuple (\theta, \phi, r)
def getReflectionPlane(v1, v2):
	v = (v2 - v1)
	vmid = v1 + v/2
	z = v[2]
	y = v[1]
	x = v[0]
	r = np.linalg.norm(vmid)
	rho = np.linalg.norm(v)
	theta = np.arcsin(z/rho)
	arccosval = x/(rho * np.cos(theta))
	if arccosval < -1:
		print arccosval
	 	arccosval = -1
	if arccosval > 1:
		print arccosval
		arccosval = 1
	phi = np.arccos(arccosval)
	if y < 0:
		phi = 2*np.pi - phi
	if theta < 0:
		theta = -theta
		phi += np.pi
		phi = phi % (np.pi*2)
	return (theta, phi, r)

def main():
	X = np.load('../data/NumpyArrays/data_0.npy')
	X = X[1:4,:]
	np.random.seed(5)
	PRST = PRST_3D(X, 100, 10)
	print PRST

if __name__ == "__main__":
    main()
