import math
import sys
sys.path.append("S3DGLPy")
from Primitives3D import *
from PolyMesh import *
import numpy as np
import Queue


### Purpose: Finds the All-Pairs Geodesic Distance for a give mesh
###          Using the Floyd-Warshall Algorithm
def geodesicDistance(mesh):
	return floydWarshall(createGraph(mesh))

def floydWarshall(distance):
	print "Finding All-pairs geodesic distance by Floyd-Warshall Algorithm"
	print distance.shape
	nodes = range(distance.shape[0])
	for k in nodes:
		print k
		for i in nodes:
			for j in nodes:
				if distance[i][k] + distance[k][j] < distance[i][j]:
					distance[i][j] = distance[i][k] + distance[k][j]
	return distance

def createGraph(mesh):
	graph = np.inf*np.ones((mesh.VPos.shape[0],mesh.VPos.shape[0]))
	np.fill_diagonal(graph, 0)

	for i in mesh.vertices:
		neighbors = mesh.vertices[i.ID].getVertexNeighbors()
		for j in neighbors:
			vec = mesh.VPos[i.ID] - mesh.VPos[j.ID]
			vec = vec.astype(float)
			distanceIJ = np.linalg.norm(vec)
			graph[i.ID][j.ID] = distanceIJ

	return graph

if __name__ == '__main__':
	m = PolyMesh()
	filename = "../../../Laplacian Meshes/meshes/bird.off"
	m.loadFile(filename)
	geodesicDistance(m)
