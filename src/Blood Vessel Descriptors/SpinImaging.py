import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm, colors

#Purpose: To compute PCA on a point cloud
#Inputs: X (3 x N array representing a point cloud)
def doPCA(X):
    ##TODO: Fill this in for a useful helper function

    # Compute covariance matrix A = X.T * X
    A = X.dot(X.T)
    # Compute eigenvalues/eigenvectors of A, sorted in decreasing order

    eigenValues, eigenVectors = np.linalg.eig(A)

    idx = eigenValues.argsort()[::-1]
    eigenValues = eigenValues[idx]
    eigenVectors = eigenVectors[:,idx]

    return (eigenValues, eigenVectors)

#Purpose: To create an image which stores the amalgamation of rotating
#a bunch of planes around the largest principal axis of a point cloud and
#projecting the points on the minor axes onto the image.
#Inputs: Ps (3 x N point cloud), Ns (3 x N array of normals, not needed here),
#NAngles: The number of angles between 0 and 2*pi through which to rotate
#the plane, Extent: The extent of each axis, Dim: The number of pixels along
#each minor axis
def getSpinImageFast(Ps, Ns, NAngles, Extent, Dim):
    #Create an image
    hist = np.zeros((Dim, Dim))

    # Project all points on PCA Axis
    bins = np.linspace(0, Extent, num = Dim+1)
    eigVal, eigVec = doPCA(Ps)
    pAxis  = eigVec[:,0]

    projPs = np.asarray((Ps.T.dot(pAxis)) / pAxis.T.dot(pAxis))[:,0]
    perpProj = Ps - pAxis.dot(pAxis.T).dot(Ps)
    mags     = np.sqrt(np.einsum("ji,ji->i", perpProj, perpProj))

    heatmap, xedges, yedges = np.histogram2d(projPs,mags,bins=(bins,bins),normed=True)

    plt.clf()
    plt.imshow(heatmap)
    plt.show()

    return np.histogram2d(mags,projPs,bins=(bins,bins),normed=True)[0].flatten()

def spinFastWithAxes(segmentList, Extent, Dim):
    #Create an image
    hist = np.zeros((Dim, Dim))
    binsX = np.linspace(0, Extent, num = Dim+1)
    binsY = np.linspace(0, 1, num = Dim+1)
    prevLen = 0.0

    allProjPs = np.empty(0)
    allMags   = np.empty(0)

    for i in xrange(len(segmentList)):

        pAxis = segmentList[i][0][1] - segmentList[i][0][0]
        pAxis = np.asmatrix(pAxis).T

        Ps    = np.asmatrix(segmentList[i][1].T) - np.asmatrix(segmentList[i][0][0,:]).T

        if Ps.shape[1] != 0:

            #Get parallel projection and correct for the length of previous segments
            projPs = np.asarray((Ps.T.dot(pAxis)) / pAxis.T.dot(pAxis))[:,0]
            projPs = projPs + prevLen

            #Get perpendicular projections' magnitudes
            perpProj = Ps - pAxis.dot(pAxis.T).dot(Ps)
            mags     = np.sqrt(np.einsum("ji,ji->i", perpProj, perpProj))

            allProjPs = np.concatenate((allProjPs, projPs))
            allMags = np.concatenate((allMags, mags))

        #Adjust prevLen to include current segment
        prevLen += np.sqrt(pAxis.T.dot(pAxis))[0,0]


    heatmap, xedges, yedges = np.histogram2d(allProjPs, allMags,bins=(binsX,binsY),normed=True)
    plt.clf()
    plt.imshow(heatmap)
    plt.show()
    return np.histogram2d(allProjPs,allMags,bins=(binsX,binsY),normed=True)[0].flatten()
