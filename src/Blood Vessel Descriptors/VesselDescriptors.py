#Purpose: To implement a suite of 3D shape statistics and to use them for point
#Blood Vessel Classification
import sys
sys.path.append("S3DGLPy")
from PolyMesh import *
from Primitives3D import *

import numpy as np
import PRST as prst
import SpinImaging as sp
import SphericalHarmonics as sh

from random import randint


#########################################################
##                UTILITY FUNCTIONS                    ##
#########################################################

#Purpose: Export a sampled point cloud into the JS interactive point cloud viewer
#Inputs: Ps (3 x N array of points), Ns (3 x N array of estimated normals),
#filename: Output filename
def exportPointCloud(Ps, Ns, filename):
    N = Ps.shape[1]
    fout = open(filename, "w")
    fmtstr = "%g" + " %g"*5 + "\n"
    for i in range(N):
        fields = np.zeros(6)
        fields[0:3] = Ps[:, i]
        fields[3:] = Ns[:, i]
        fout.write(fmtstr%tuple(fields.flatten().tolist()))
    fout.close()

def centerAndScale(Ps):
    # Recenter and scale points taken from voxel data
    c = np.asmatrix([list(np.mean(Ps, axis=1))]).T

    Ps = Ps - c
    # Calculate scale
    squares = list(np.einsum("ji,ji->i", Ps, Ps))
    sums    = np.sum(squares)
    scale   = math.sqrt(sums/len(squares))
    # Apply Scale
    Ps      = Ps / scale

    return Ps

#Purpose: To sample a point cloud, center it on its centroid, and
#then scale all of the points so that the RMS distance to the origin is 1
def samplePointCloud(mesh, N):
    (Ps, Ns) = mesh.randomlySamplePoints(N)

    Ps = centerAndScale(Ps)

    return (Ps, Ns)


#########################################################
##              SYMMETRY COMPARISONS                  ##
#########################################################

#Takes in point cloud Ps and a line from p0 to p1, centers Ps on the midpoint
#of p0 and p1. It then computes the standard deviation of the distance from
#points in Ps to the line. Higher SD = less rotational symmetry around line.
#Function returns a tuple (StandardDeviation, CylinderRadius)
#IMPORTANT: Ps, p0, and p1 must use the same coodinate system.
def getCylindricalSymmetry(Ps, p0, p1):
    Ps = np.asmatrix(Ps).T
    p0 = np.asmatrix(p0).T
    p1 = np.asmatrix(p1).T

    origin = (p0 + p1) / 2
    Ps = Ps - origin
    p0 = p0 - origin
    p1 = p1 - origin
    v  = p1 - p0
    perpProj = Ps - v.dot(v.T).dot(Ps)
    mags     = np.sqrt(np.einsum("ji,ji->i", perpProj, perpProj))

    SD = np.std(mags)
    R  = np.mean(mags)

    return np.array([SD, R])

def spinOnAxes(segmentList,NS, Extent, Dim):
    return sp.spinFastWithAxes(segmentList, Extent, Dim)

def spinImage(Ps, Ns, NAngles, Extent, Dim):
    return sp.getSpinImageFast(Ps, Ns, NAngles, Extent, Dim)

def sphericalHamornicMagnitudes(Ps, Ns, RMax, NHarmonics, NSpheres, F, res):
    return sh.getSphericalHarmonicMagnitudes(Ps, Ns, RMax, NHarmonics, NSpheres, F, res)

def PRST(Ps,Ns, M, K):
    return prst.PRST_3D(Ps, M, K).flatten()

#########################################################
##              HISTOGRAM COMPARISONS                  ##
#########################################################
#Purpose: To compute the euclidean distance between a set of 2D histograms
def compare2DHistsEuclidean(AllHists):
    AllHists = np.asarray(AllHists)

    N = AllHists.shape[2]
    D = np.zeros((N, N))

    dotX = np.sum(AllHists**2, (1,2))[:, None]
    dotY = np.sum(AllHists**2, (1,2))[None, :]
    dots = np.einsum("ikl,jkl->ij", AllHists, AllHists)
    D = dotX + dotY - 2*dots

    D[D < 0.5] = 0

    return np.sqrt(D)
#Purpose: To compute the euclidean distance between a set
#of histograms
#Inputs: AllHists (K x N matrix of histograms, where K is the length
#of each histogram and N is the number of point clouds)
#Returns: D (An N x N matrix, where the ij entry is the Euclidean
#distance between the histogram for point cloud i and point cloud j)
def compareHistsEuclidean(AllHists):
    N = AllHists.shape[1]
    D = np.zeros((N, N))
    #TODO: Finish this, fill in D
    dotX = np.sum(AllHists**2, 0)[:, None]
    dotY = np.sum(AllHists**2, 0)[None, :]
    D = dotX + dotY - 2*AllHists.T.dot(AllHists)

    D[D < 0.5] = 0

    return np.sqrt(D)

#Purpose: To compute the cosine distance between a set
#of histograms
#Inputs: AllHists (K x N matrix of histograms, where K is the length
#of each histogram and N is the number of point clouds)
#Returns: D (An N x N matrix, where the ij entry is the cosine
#distance between the histogram for point cloud i and point cloud j)
def compareHistsCosine(AllHists):
    N = AllHists.shape[1]
    D = np.zeros((N, N))
    #TODO: Finish this, fill in D
    num = AllHists.T.dot(AllHists)
    mag = np.asmatrix([list(np.sqrt(np.einsum("ji,ji->i", AllHists, AllHists)))])
    den = mag.T.dot(mag)

    return np.arccos(num/den)

#Purpose: To compute the chi squared distance between a set
#of histograms
#Inputs: AllHists (K x N matrix of histograms, where K is the length
#of each histogram and N is the number of point clouds)
#Returns: D (An N x N matrix, where the ij entry is the chi squared
#distance between the histogram for point cloud i and point cloud j)
def compareHistsChiSquared(AllHists):
    shape = (AllHists.shape[1], AllHists.shape[1])
    def chiSquaredDist(a,b):
        h1 = AllHists[:,a]
        h2 = AllHists[:,b]
        f = np.vectorize(indvChiSquared)
        return np.sum(f(h1.flatten(), h2.flatten()), dtype=float)
    def indvChiSquared(a, b):
        n = 2 * np.square(a - b)
        d = a + b
        if n ==0:
            return 0
        return (n / float(d))

    f = np.vectorize(chiSquaredDist)
    x = np.fromfunction(lambda i, j: f(i, j), shape, dtype=int)
    return x

def compareHistsEMD1D(AllHists):
    print AllHists.shape
    N = AllHists.shape[1]
    K = AllHists.shape[0]
    CS = np.cumsum(AllHists, 0)
    D = np.zeros((N, N))
    for k in range(K):
        c = CS[k, :]
        D += np.abs(c[:, None] - c[None, :])
    return D

def compareHistsEMD2D(AllHists):
    N = AllHists.shape[0]
    I = AllHists.shape[1]
    J = AllHists.shape[2]
    D = np.zeros((N ,N))
    CS = np.cumsum(AllHists, 1)
    CS = np.cumsum(CS, 2)
    for i in xrange(I):
        for j in xrange(J):
            c = CS[:,i,j]
            D += np.abs(c[:, None] - c[None, :])
    return D

def compareHistsSpinEuclidean(AllHists, Dim):
    N = AllHists.shape[1]
    D = np.zeros((N, N))
    for i in range(N):
        I = np.reshape(AllHists[:, i], [Dim, Dim])
        for j in range(N):
            J = np.reshape(AllHists[:, j], [Dim, Dim])
            J2 = np.flipud(J)
            d1 = np.sqrt(np.sum((I-J)**2))
            d2 = np.sqrt(np.sum((I-J2)**2))
            #print d1, ", ", d2
            D[i, j] = min(d1, d2)
    return D

#Purpose: Utility function for wrapping around the statistics functions.
#Inputs: PointClouds (a python list of N point clouds), Normals (a python
#list of the N corresponding normals), histFunction (a function
#handle for one of the above functions), *args (addditional arguments
#that the descriptor function needs)
#Returns: AllHists (A KxN matrix of all descriptors, where K is the length
#of each descriptor)
def makeAllHistograms(PointClouds, Normals, histFunction, *args):
    N = len(PointClouds)
    #Call on first mesh to figure out the dimensions of the histogram
    print len(PointClouds)
    h0 = histFunction(PointClouds[0], Normals[0], *args)
    K = h0.size
    AllHists = np.zeros((K, N))
    AllHists[:, 0] = h0
    for i in range(1, N):
        print "Computing histogram %i of %i..."%(i+1, N)
        AllHists[:, i] = histFunction(PointClouds[i], Normals[i], *args)
    return AllHists

def makeAll2DHistograms(PointClouds, Normals, histFunction, *args):
    N = len(PointClouds)
    #Call on first mesh to figure out the dimensions of the histogram
    h0 = histFunction(PointClouds[0], Normals[0], *args)
    K = h0.shape[0]

    AllHists = np.zeros((N,K, K))
    AllHists[0,:] = h0

    for i in range(1, N):
        print "Computing histogram %i of %i..."%(i+1, N)
        AllHists[i,:] = histFunction(PointClouds[i], Normals[i], *args)

    return AllHists

def loadSegmentsPointClouds(PointClouds,Normals,pc):
    # Computes spin imaging on all segments of a particular voxel point cloud
    filename = "../data/NumpyArrays/data_%d.npy"%(pc)
    Ps = np.load(filename) # Load data from the numpy parsed
    selection = [randint(0,Ps.shape[1]-1) for p in range(0, NSamples)]
    Ps = Ps[:,selection]

    for x in xrange(1,2):
        # Load and bin points by segment on centerline
        filename1 = "../data/Centerlines/m1Segment_%d.npy"%(x)
        filename2 = "../data/Centerlines/m2Segment_%d.npy"%(x)
        # Centerline
        E2 = np.load(filename1) # Load data from the numpy parsed files
        S2 = np.load(filename2) # Load data from the numpy parsed

        linPts = binPoints(E2[:3,:], S2[:3,:], Ps[1:4,:])
        print len(linPts)
        for i in xrange(len(linPts)):
            # Compute results on each segment
            nPs = np.asarray(linPts[i][1].T.tolist())
            nPs = centerAndScale(nPs)

            PointClouds.append(nPs)
            Normals.append(nPs)

    return PointClouds, Normals

def loadAllPointClouds(PointClouds, Normals):

    for x in [0,10,20,30,40,47,55,65]:
        filename = "../data/NumpyArrays/data_%s.npy"%(x)
        Ps = np.load(filename) # Load data from the numpy parsed files

        selection = [randint(0,Ps.shape[1]-1) for p in range(0, NSamples)]

        Ps = Ps[:,selection]           # Limit the number of points
        Ps = centerAndScale(Ps[1:4,:]) # Only look at the vocel data as points

        PointClouds.append(Ps)
        Normals.append(Ps)

    return PointClouds, Normals

def loadSegmentOfAllPointClouds(PointClouds, segNum):

    segments = []

    for x in [0,10,20,30,40,47,55,65]:
        print x
        filename = "../data/Centerlines/binnedPoints_%d.npy"%(x)
        linePts = np.load(filename) # Load data from the numpy parsed files

        segment = []
        for i in xrange(len(linePts)):
            line = linePts[i]

            if line[2] == segNum:
                segment.append(line)

        segments.append(segment)

    return segments
#########################################################
##                     MAIN TESTS                      ##
#########################################################\
# Purpose: Computes SphermicalHarmonics on point clouds for all voxel data
def computeSphermicalHarmonics(NSamples,PointClouds,Normals):

    PointClouds, Normals = loadAllPointClouds(PointClouds, Normals)

    NHarmonics = 20
    NSpheres = 32   # value used by the paper
    res = 3
    # take the magnitudes and drop the phase
    F = sh.computeFMatrix(NHarmonics, res, True)

    HistsSpherical = makeAllHistograms(PointClouds, Normals, sphericalHamornicMagnitudes, 2, NHarmonics, NSpheres, F, res)
    DShape_E1      = compareHistsEuclidean(HistsSpherical)
    print DShape_E1

# Purpose: Computes Spin Imaging on point clouds for all voxel data broken into segments
def computeSphermicalHarmonicsOnSegment(NSamples,PointClouds,Normals,segment):
    PointClouds, Normals = loadSegmentsPointClouds(PointClouds,Normals,segment)

    NHarmonics = 20
    NSpheres = 32   # value used by the paper
    res = 3
    # take the magnitudes and drop the phase
    F = computeFMatrix(NHarmonics, res, True)

    HistsSpherical = makeAllHistograms(PointClouds, Normals, sphericalHamornicMagnitudes, 2, NHarmonics, NSpheres, F, res)
    DShape_E1      = compareHistsEuclidean(HistsSpherical)
    print DShape_E1

# Purpose: Computes Spin Imaging on point clouds for all voxel data
def computeSpinImage(NSamples,PointClouds,Normals):

    PointClouds, Normals = loadAllPointClouds(PointClouds, Normals)

    HistsSpin = makeAllHistograms(PointClouds, Normals, spinImage, 100, 2, 40)
    #DShape_E1 = compareHistsSpinEuclidean(HistsSpin, 40)
    DShape_E1 = compareHistsEMD1D(HistsSpin)

    print DShape_E1

# Purpose: Computes Spin Imaging on point clouds for all voxel data broken into segments
def computeSpinImageOnSegments(PointClouds,Normals,segment):
    # Computes spin imaging on all segments of a particular voxel point cloud
    PointClouds, Normals = loadSegmentsPointClouds(PointClouds,Normals,pc)
    HistsSpin = makeAllHistograms(PointClouds, Normals, spinImage, 100, 2, 40)
    DShape_E1 = compareHistsEMD1D(HistsSpin)
    #print DShape_E1

# Purpose: Computes Spin Imaging on point clouds for all voxel data broken into segments
def computePRST(PointClouds,Normals,M):
    # Computes spin imaging on all segments of a particular voxel point cloud
    PointClouds, Normals = loadAllPointClouds([], [])
    HistsPRST = makeAllHistograms(PointClouds, Normals, PRST, M, 40)
    DShape_E1 = compareHistsEMD1D(HistsPRST)
    print DShape_E1



if __name__ == '__main__':
    NSamples = 100000 # How many points do you want to sample

    # What do you want to do
    if sys.argv[1] == 'spin':
        computeSpinImage(NSamples,[],[]) # Compute on normal voxel data

    elif sys.argv[1] == 'sphere':
        computeSphermicalHarmonics(NSamples,[],[])

    elif sys.argv[1] == 'prst':

        M = 100
        computePRST([],[],M)

    elif sys.argv[1] == 'cylinder':

        data = int(raw_input("Which dataset would you like to test: 0,10,20,30,40,47,55,65: "))
        ## Line segment for spin fast with axis / other segment optioned
        if data not in [0,10,20,30,40,47,55,65]:
            data = 0
        # Set to voxel mesh 0 for testing
        filename = "../data/Centerlines/binnedPoints_%d.npy"%(data)
        linePts = np.load(filename)

        results = np.zeros((0,4))
        for i in xrange(len(linePts)):
            line = linePts[i]
            result = getCylindricalSymmetry(line[1], line[0][0], line[0][1])
            result = np.asmatrix(np.append(result,[line[2],i]))
            results = np.concatenate((results,result))
            #print "Segment #%d:%d --- SD: %f | R %f"%(line[2],i,result[0,0],result[0,1])

        maxes  = np.argmax(results, axis=0)
        maxR   = results[maxes[0,1],:]
        maxSD  = results[maxes[0,0],:]
        mins   = np.argmin(results, axis=0)
        minR   = results[mins[0,1],:]
        minSD  = results[mins[0,0],:]

        print "MIN Radius| Segment #%d:%d\t --- SD: %f | R %f"%(minR[0,2],minR[0,3],minR[0,0],minR[0,1])
        print "MAX Radius| Segment #%d:%d\t --- SD: %f | R %f"%(maxR[0,2],maxR[0,3],maxR[0,0],maxR[0,1])
        print "MIN SD    | Segment #%d:%d\t --- SD: %f | R %f"%(minSD[0,2],minSD[0,3],minSD[0,0],minSD[0,1])
        print "MAX SD    | Segment #%d:%d\t --- SD: %f | R %f"%(maxSD[0,2],maxSD[0,3],maxSD[0,0],maxSD[0,1])


    elif sys.argv[1] == 'spinA' or sys.argv[1] == 'spina' :
        segNum = int(raw_input("Which segment do you want to test: 1-9: "))

        ## Line segment for spin fast with axis / other segment optioned
        if segNum > 10 or segNum < 1:
            segNum = 1

        data = int(raw_input("Which dataset would you like to test: 0,10,20,30,40,47,55,65: "))
        ## Line segment for spin fast with axis / other segment optioned
        if data not in [0,10,20,30,40,47,55,65]:
            data = 0
        # Set to voxel mesh 0 for testing
        filename = "../data/Centerlines/binnedPoints_%d.npy"%(data)
        linePts = np.load(filename)

        segment = []
        for i in xrange(len(linePts)):
            line = linePts[i]

            if line[2] == segNum:
                segment.append(line)

        sp.spinFastWithAxes(segment, 2, 40)
    else:
        segNum = int(raw_input("Which segment do you want to test: 1-9: "))

        ## Line segment for spin fast with axis / other segment optioned
        if segNum > 10 or segNum < 1:
            segNum = 1
        PointClouds = loadSegmentOfAllPointClouds([], segNum)
        
        HistsSpin   = makeAllHistograms(PointClouds, PointClouds, spinOnAxes, 2, 40)
        DShape_E1   = compareHistsEMD1D(HistsSpin)
        print DShape_E1
        #print "Options:\n (1)'spin'\n (2)'sphere'\n (3)'cylinder'\n  (3)'prst'\n (4)'spina'"
