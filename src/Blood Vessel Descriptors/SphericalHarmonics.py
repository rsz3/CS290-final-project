import numpy as np
import sys
sys.path.append("S3DGLPy")
from Primitives3D import *
from PolyMesh import *
from scipy.special import sph_harm

def convertPolarToRectangular(r, phi, theta):
    x = r * np.cos(theta) * np.sin(phi)
    y = r * np.sin(theta) * np.sin(phi)
    z = r * np.cos(phi)
    return [x, y, z]

def convertRectangularToPolar(x, y, z):
    if(x == 0 and y>0):
        theta = np.pi / 2
    elif (x == 0):
        theta = -np.pi/2
    else:
        theta = np.arctan(y/x)

    if(x < 0):
        theta = theta + np.pi

    phi = np.arccos(z/np.sqrt(x*x + y*y + z*z))

    return [theta,phi]
#Purpose: To sample the unit sphere as evenly as possible.  The higher
#res is, the more samples are taken on the sphere (in an exponential
#relationship with res).  By default, samples 66 points
def getSphereSamples(res = 2):
    m = getSphereMesh(1, res)
    return m.VPos.T
# computes the spherical harmonic F matrix
# as discussed in the paper
def computeFMatrix(NHarmonics, res=2, magnitude = True):
    SPoints = getSphereSamples(res)
    B = SPoints.shape[1]

    # vertex n = SPoints[:,n]

    # here we construct the Bs matrix, which is a Bx2 matrix containing the theta, phi values for every point on the sampled sphere (converting rectantular to polar)
    Bs = np.zeros((B, 2))
    for i in range(0, B):
        x = SPoints[0, i]
        y = SPoints[1, i]
        z = SPoints[2, i]
        [theta, phi] = convertRectangularToPolar(x, y, z)
        Bs[i] = np.array([theta, phi])

    # Calculate the spherical harmonics matrix
    F = np.zeros((NHarmonics, B))+0j
    for m in range(0, NHarmonics):
        # the paper ignores "l" (the degree) by summing up degrees from -m to m
        # for some reason the equation in the paper also uses the absolute value of l
        F0 = sph_harm(-m, m, Bs[:,0] , Bs[:,1])
        phi = np.linspace(0, np.pi, 100)
        theta = np.linspace(0, 2*np.pi, 100)
        phi, theta = np.meshgrid(phi, theta)
        x = np.sin(phi) * np.cos(theta)
        y = np.sin(phi) * np.sin(theta)
        z = np.cos(phi)

        for l in range(-m+1, m+1):
            F0 += sph_harm(np.abs(l), m, Bs[:,0] , Bs[:,1])

        F[m, :] = F0

    if(magnitude):
        F = np.absolute(F)

    return F

#Purpose: To create a histogram of spherical harmonic magnitudes in concentric spheres
#Inputs: Ps (3 x N point cloud), Ns (3 x N array of normals, not used here), RMax: maximum
# radius, NHarmonics: the number of spherical harmonics, NSpheres: the number of spheres,
# F: the Spherical Harmonic matrix, res: resolution for sphere sampling
def getSphericalHarmonicMagnitudes(Ps, Ns, RMax, NHarmonics, NSpheres, F, res):
    SPoints = getSphereSamples(res)
    B = SPoints.shape[1]

    # now we compute h (a BxN matrix, B = number of sampled sphere points, N = number of shells)(see getShapeShellHistogram for detailed comments)
    dots = np.dot(Ps.T, SPoints)
    maximums = np.argmax(dots,axis=1).T

    maximums = np.asarray(maximums)
    h = np.zeros((NSpheres, 0))
    for i in range(B):
        # # # index of every element where i == us
        sectorElems = Ps[:,maximums.flatten() == i] # Select every element in the given sector
        sectorHistogram = np.array(np.histogram(np.linalg.norm(sectorElems,axis=0), NSpheres, (0,RMax))[0])[np.newaxis].T # Create a histogram for the sector in Column form
        h=np.hstack((h,sectorHistogram)) # Add the column to the histogram

    # take the transpose for multiplication by the F matrix
    h = h.T

    # in the paper they stored only a 1 if there were any points in the voxel and a 0 if there weren't
    # we rasterize the point cloud to a spherical space, rather than the paper's cartesian coordinates, using the ShapeShell histogram
    h[np.where(h>0)] = 1

    # finally, we compute the spherical harmonic coefficient matrix, which is the descriptor
    H = F.dot(h)

    return H.flatten()
