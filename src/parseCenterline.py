import numpy as np
import re

def simplifyCenterline(Ps):
    if Ps.shape[1] < 3:
        return Ps
    i = 0
    while i < Ps.shape[1] - 2:
        pt0 = Ps[:,i][0:3]
        pt1 = Ps[:,i+1][0:3]
        pt2 = Ps[:,i+2][0:3]

        #Get appropriate vectos Ps0 -> Ps1 and Ps1 -> Ps2
        vec01 = pt1 - pt0
        vec12 = pt2 - pt1

        ### theta = a dot b / ||a||||b|| ###

        # Dot product
        num = vec01.dot(vec12.T) # 1xN array

        # Magnitudes of vectors
        mag01 = np.sqrt(vec01.dot(vec01.T))
        mag12 = np.sqrt(vec12.dot(vec12.T))

        den = mag01 * mag12 #1xN

        CosThetas = num / den
        ArcCosT = np.arccos(CosThetas)

        # If the angle is less than 30 degrees then its straight enough. Delete it
        if ArcCosT < 0.52:
            Ps = np.delete(Ps, i+1, 1)
        i = i + 2
    return Ps

def parseData(fpath):
  f = open(fpath, 'r')
  num_lines = sum(1 for line in f)

  C = np.zeros([21, num_lines])

  with open(fpath) as openfileobject:
      index = 0
      for line in openfileobject:
        line = line.split()
        ## remove 'n/a's
        for ind, item in enumerate(line):
          if item == "n/a":
              line[ind] = np.nan

        lineArray = np.array(line)

        C[:,index] = lineArray
        index += 1

  return C

def parseSegments(C):
     X = np.zeros([21, C.shape[1] - 1])
     Y = np.zeros([21, C.shape[1] - 1])

     for i in xrange(C.shape[1]):
         if i == 0:
             X[:,i] = C[:,i]
         elif i == C.shape[1] - 1:
             Y[:,i - 1] = C[:,i]
         else:
             X[:,i] = C[:,i]
             Y[:,i - 1] = C[:,i]
     return X, Y

def main():
  for x in xrange(1,10):
    C    = parseData( "./data/Normals/CxPt_Normal_%d.txt" % x )
    sC   = C#simplifyCenterline(C)
    X, Y = parseSegments(sC)

    np.save("./data/Centerlines/centerLine%d.npy"  % x, C)
    np.save("./data/Centerlines/centerLineS%d.npy" % x, sC)
    np.save("./data/Centerlines/m1Segment_%d.npy"  % x, X)
    np.save("./data/Centerlines/m2Segment_%d.npy"  % x, Y)
    print "Done with %d!" % x

if __name__ == "__main__":
    main()
