import numpy as np
import re

def parseData(fpath):
  f = open(fpath, 'r')
  num_lines = sum(1 for line in f)
  X = np.empty([9, num_lines])
  with open(fpath) as openfileobject:
      index = 0
      for line in openfileobject:
        lineArray = re.findall(r"[a-zA-Z0-9+-.]+", line)
        lineArray = np.array(lineArray)
        X[:,index] = lineArray

        index+=1

  return X

def main():
  vals = map( str, [0, 10, 20, 30, 40, 47, 55, 65] )
  for key,value in enumerate(vals):
    X = parseData( "data/embc2/%s/lbm.iter00150000/data.txt" % value )
    np.save("data_%s.npy" % value, X)
    print "Done with %s!" % value

if __name__ == "__main__":
    main()
