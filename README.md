# 3D Blood Vessel Shape Statstics - Final Project #
- Roger Zou
- Weston Carvalho
- Aaron Liberatore

### Final Project ###

The presentation summary is in `presentation/summary.pdf`

Since the milestone, we worked on first cutting up the blood vessel segments in order to deal with segments at a time. To do this we first turn all of the centerline data into numpy arrays, then create two separate arrays detailing corresponding start and end points. Following this, we bin points according to the average distance to each segment in `pointBinner.py`. This allows us to apply our testing features directly to a particular segment in question, which will heighten our degree of sensitivity in testing, whereas merely looking at the whole blood vessel may really not show the degree of specificity needed to identify clogging prone vessels

We also implemented PRST, cylindrical testing, and variants of the spin images. Instead of looking at each segment or the whole blood vessel from its PCA axis, we wrap around the sub-segments themselves. This allows us to unwrap curled branches.

We compared the varying point clouds using mostly euclidean distances and cosine distances. Additionally we implemented a 2d Earth Movers distance that would better take into account the 2D nature of the spin imaging histograms.

Using PRST, spin images, and spherical harmonics we attempted to find a correlation between different voxel grids that might suggest a particular aorta blood vessel would be prone to clogging. John Gounley mentioned that the segments prone to plaques are the inner curvature of the aortic arch and the inner curvature of the descending aorta. Using this information, we geared our testing to look at these segments in particular. Though we are not positive on which data set would reflect a likely candidate for plague buildup.

After running our cylindrical code on each set of binned points we found that Segment #9 between subsections 280-300 consistently showed the minimum radius, but also the minimum SD. Since we randomly select 500k points, the subsections change. Additionally, Segment #4 subsection 77-78 always has the highest radius with the largest standard deviation. These findings are interesting because Segment #9 connects into the much larger segments 1 and 2 suggesting a likely location for blockage to occur has blood funnels into a smaller branch.

Example: (Additionally raw data found in ./SRC/Data/Results/RawData.txt)
MIN Radius| Segment #9:273	 --- SD: 0.007782 | R 0.020227
MAX Radius| Segment #4:78	 --- SD: 0.048007 | R 0.136328
MIN SD    | Segment #9:271	 --- SD: 0.006204 | R 0.021816
MAX SD    | Segment #4:77	 --- SD: 0.053411 | R 0.136154

Our segment to segment comparisons also present interesting information. Since the mesh itself is slightly larger only in particular areas of the mesh, when we look at segment-segment comparisons this information is reflected in their euclidean distances.

Take, for example, segment 9 comparisons and segment 8 comparisons (simplified to just show column 1)

Segment 9                        Segment 8
0 : [[     0.                        [[    0.  
10: [  3786.85589905                 [ 2987.64369827
20: [  8893.33136378                 [ 2857.5074307
30: [  2166.21545514                 [ 5486.61037447  
40: [  6375.38490507                 [ 5810.19067681
47: [  3604.78374828                 [ 1427.7139165    # Problem with voxel data for data_47 for unknown reason
55: [ 13294.74508139                 [ 7377.34202784           
65: [  8536.63385546                 [ 6291.65417213

Whereas segment 9 shows a relative similarity among many of the others voxel sets, data_0 is incredibly different from data_55. This difference is not reflected in other segment tests, as shown in the segment 8 tests that showed relatively similar results. We were not 100% which branches aligned with the aortic arch and descending arch, but this testing could be used given that information to classify data sets with similar.


Following this up, we began the implementation of canonical forms by efficiently building an all-pairs geodesic distance matrix, but have not made much more progress on the canonical form algorithm itself.


### INSTRUCTIONS ###

(0) Setup
    (a) Download original 3.7 GB data as `data` within `src/` directory
    (b) Create empty `Centerlines` , `NumpyArrays` , `Normals` folders inside `src/data/`
    (c) Run the follow functions from `src`
        - `python parseData.py`       -- puts all data into numpy files
        - `python parseCenterline.py` -- puts all centerline info into numpy files
        - `python pointBinner.py`     -- bins all points according to their closest centerline

(1) go to `../src/Blood\ Vessel\ Descriptors/`

(2) run `python VesselDescriptors.py   arg`

    With argument of either:
    1) `spin`      -- (spin image on all voxel data)
    2) `sphere`    -- (spherical harmonics on all voxel data)
    3) `prst`      -- (prst on all voxel data)
    4) `cylinder`  -- (cylindrical information on all subsegments of specified )
    5) `spina`     -- (spin image around centerline axis on all voxel data)
    6) no argument gives Euclidean distance for Spin image around axis with specified a line segment

(3) Follow prompts
